from django.shortcuts import render

# Before refactoring, we create HttpResponse manually
# from django.http import HttpResponse

# Create your views here.
def home_page(request):

	# Before Refactoring
	# return HttpResponse('<html><title>To-Do lists</title></html>')

	# After Refactoring
	return render(request, 'home.html')