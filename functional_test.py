from selenium import webdriver
import unittest
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.keys import Keys
class NewVisitorTest(unittest.TestCase):

	def setUp(self):
		self.browser = webdriver.Firefox()
		

	def tearDown(self):
		self.browser.quit()

	def test_can_start_a_list_and_retrieve_it_later(self):
		# Edith has heard about a cool new online to-do app. She goes
		# to check out its homepage
		self.browser.get('http://localhost:8000')

		# She notices the page title and header mention to-do lists
		self.assertIn('To-Do', self.browser.title)
		header_text = self.browser.find_element_by_tag_name('h1').text
		# Yoga - disini ngecek apakah text yang ada tag 'h1'nya merupakan 'To-Do' -
		self.assertIn('To-Do',header_text)

		# She is invited to enter a to-do item straight away
		inputbox = self.browser.find_element_by_id('id_new_item')
		# Yoga - disini ngecek apakah placeholdernya enter a to-do item -
		self.asserEqual(inputbox.get_attribute('placeholder'),'Enter a to-do item')
		
		# Yoga appear!!
		# Yoga wants to click the inputbox
		actions = ActionChains(driver)
		actions.click(inputbox)

		# She types "Buy peacock feathers" into a text box (Edith's hobby
		# is tying fly-fishing lures)
		inputbox.send_keys('Buy peacock feathers')

		# When she hits enter, the page updates, and now the page lists
		inputbox.send_keys(Keys.ENTER)
		# "1: Buy peacock feathers" as an item in a to-do list
		table = self.browser.find_element_by_id('id_list_table')
		rows = table.find_element_by_tag_name('tr')
		# disini pake row.text, bawaannya python yang bisa ngakses table dari rows
		self.assertTrue(any(row.text == '1: Buy peacock feathers' for row in rows))

		# There is still a text box inviting her to add another item. She
		# enters "Use peacock feathers to make a fly" (Edith is very methodical)
		inputbox.send_keys('Use peacock feathers to make a fly')
		inputbox.send_keys(Keys.ENTER)
		# The page updates again, and now shows both items on her list
		self.assertTrue(any(row.text == '2: Use peacock feathers to make a fly' for row in rows))
		# Edith wonders whether the site will remember her list. Then she sees
		# that the site has generated a unique URL for her -- there is some
		# explanatory text to that effect.

		# She visits that URL - her to-do list is still there.

		# Satisfied, she goes back to sleep
if __name__ == '__main__':
	unittest.main(warnings='ignore')