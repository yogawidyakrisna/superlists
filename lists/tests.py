from django.core.urlresolvers import resolve
from django.test import TestCase
from django.http import HttpRequest
from lists.views import home_page
from django.template.loader import render_to_string

# Create your tests here.
class HomePageTest(TestCase):

	# Check if the root of site have a function 'home_page'
	def test_root_url_resolves_to_homepage_view(self):
		found = resolve('/')
		self.assertEqual(found.func, home_page)

	# Check if 'home_page' function return a correct home.html structure 
	def test_home_page_returns_correct_html(self):
		request = HttpRequest()
		response = home_page(request)

		# Before refactoring (use Django render) on the view.py
		# self.assertTrue(response.content.startswith(b'<html>'))
		# self.assertIn(b'<title>To-Do lists</title>', response.content)
		# self.assertTrue(response.content.endswith(b'</html>'))

		# After refactoring
		expected_html = render_to_string('home.html')
		self.assertEqual(response.content.decode(), expected_html)
		